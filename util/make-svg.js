
import pathast from 'path-ast'

export default function (ast) {
  let d = pathast.stringify(ast)
  let svg = `
{
    "type": "path",
    "path": "${d}",
    "vendor_code": ""
}
`
  return svg
}

