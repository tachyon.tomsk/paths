
import React from 'react'
import pathast from 'path-ast'
import makeSvg from '../util/make-svg'
import Button from './Button.jsx'
import LinkBtn from './LinkBtn.jsx'
import Input from './Input.jsx'
import Table from './Table.jsx'
import Stepper from './Stepper.jsx'
import Spacer from './Spacer.jsx'
import { scale } from '../data'

class Toolbar extends React.Component {

    render () {
    let props = this.props

    function toggleGrid () {
      props.toggle('grid')
    }

    function toggleSnap () {
      props.toggle('snap')
    }

    function togglePreview () {
      props.toggle('preview')
    }

    function handleParamChange (e) {
      let changeParam = {};
      changeParam[e.target.name] = parseInt(e.target.value);
      props.updateState(changeParam);
    }

    function updateZoom (val) {
      props.updateState({ zoom: val })
    }

    let svg = makeSvg(props.ast)
    let blob = new Blob([svg], { type: 'text/plain' })
    let download = (window.URL || window.webkitURL).createObjectURL( blob )

    let s = {
      container: {
        padding: scale[1],
      },
      text: {
        fontSize: 14,
      },
        input:{
          width: '80px'
        }
    }

    return (
      <div style={s.container}>
        <Table>
          {/*<Table.Cell>*/}
          {/*  <Button*/}
          {/*    active={props.grid}*/}
          {/*    onClick={toggleGrid}>*/}
          {/*    Grid {props.grid && '•'}*/}
          {/*  </Button>*/}
          {/*</Table.Cell>*/}
          {/*<Table.Cell>*/}
          {/*  <Button*/}
          {/*    active={props.snap}*/}
          {/*    onClick={toggleSnap}>*/}
          {/*    Snap {props.snap && '•'}*/}
          {/*  </Button>*/}
          {/*</Table.Cell>*/}
          {/*<Table.Cell>*/}
          {/*  <Button*/}
          {/*    active={props.preview}*/}
          {/*    onClick={togglePreview}>*/}
          {/*    Preview {props.preview && '•'}*/}
          {/*  </Button>*/}
          {/*</Table.Cell>*/}
          <Table.Cell>
            <LinkBtn
              href={download}
              download='shape.json'>
              Download
            </LinkBtn>
          </Table.Cell>
          <Table.Cell>
            <Spacer />
          </Table.Cell>
          <Table.Cell>
            <Stepper
              value={props.zoom}
              step={1}
              min={1}
              max={30}
              onChange={updateZoom} />
          </Table.Cell>
          <Table.Cell>
            <div style={s.text}>
              Zoom {props.zoom}x
            </div>
          </Table.Cell>
          <Table.Cell>
            <Spacer />
          </Table.Cell>
          <Table.Cell>
            <div style={s.text}>
              Width/Height
            </div>
          </Table.Cell>
          <Table.Cell>
            <div
                style={s.input}
            >
              <Input
                  type={'number'}
                  name={'width'}
                  defaultValue={props.width}
                  onChange={handleParamChange}
              />
            </div>
          </Table.Cell>
          <Table.Cell>
            <div style={s.input}>
                <Input
                    type={'number'}
                    name={'height'}
                    defaultValue={props.height}
                    onChange={handleParamChange}
                />
            </div>
          </Table.Cell>
          <Table.Cell fill />
        </Table>
      </div>
    )
  }

}

export default Toolbar

